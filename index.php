<?php get_header(); ?>
    <div class="grayback pad-bottom">
    <div class="container margin-bottom-0">
    <div class="row">
    <main class="col-lg-9">
    <div class="main-text">
    <div class="strichka">
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
		    <?php if(function_exists('bcn_display'))
		    {
			    bcn_display();
		    }?>
        </div></div>
    <h1>Статьи</h1>
    <div class="row">
        <?php
//        $currentPage = (get_query_var('paged')) ? get_query_var('paged') : 1;
//        $args = array('post_per_page'=> 3, 'paged => $currentPage');
//        get_posts($args);
        if(have_posts()):
            while(have_posts()): the_post();
        get_template_part('inc/postpreview');
    endwhile;?>
    </div>
    </div>




             <?php the_posts_pagination( array(
	             'mid_size' => 2,
	             'prev_text' => __( '<i class="fas fa-chevron-left"></i>', 'textdomain' ),
	             'next_text' => __( '<i class="fas fa-chevron-right"></i>', 'textdomain' ),
             ) );?>

      <?php endif; ?>
    </main>
        <aside class="col-lg-3 flex-boxing">
	    <?php get_sidebar();?>
        </aside>
    </div>
    </div>
    </div>

<?php get_footer(); ?>