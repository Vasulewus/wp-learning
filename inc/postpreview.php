<div class="card col-sm-6 col-md-4">
	<div class="back tick">
		<a href="<?php the_permalink();?>">
			<?php the_post_thumbnail(); ?>
		</a>
		<div class="text pad-top">
			<a href="<?php the_permalink();?>"><?php the_title();?></a>
            <?php if('estate' === get_post_type() || 'com_real_estate'  === get_post_type() ) : ?>
                <div class="lifehack">
                    <?php
                    if( get_field('est_done') ) {
                        ?>
                        <p><i class="fas fa-check-circle"></i> Введен в эксплуатацию</p>
                        <?php
                    } else {
	                    the_excerpt();
                    }
                    ?>
                </div>
            <?php else: ?>
                <div class="lifehack">
		            <?php the_excerpt(); ?>
                    <div class="date"><?php the_date();?></div>
                </div>
            <?php endif; ?>

		</div>
	</div>
</div>