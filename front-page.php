<?php get_header(); ?>
    <section class=" grayback">
    <div class="container c-m-b">
    <div class="row justify-content-betweenn">
    <div class="col-lg-8">
        <div class="image">
            <div>
                <img src="<?php echo get_template_directory_uri();?>/img/home.jpg" alt="лого 'Патриот'">
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="forma">
            <ul>
                <?php dynamic_sidebar('sidebar-1');?>
            </ul>
        </div>
    </div>
<div class="container">
        <div id="tabs">
            <!-- Вкладки выполнены в виде списка с якорными ссылками -->
            <div class="container change2">
                <h2 class="zagolovok">Жилая недвижимость</h2>
                <ul class="tabs smallMenu flex-colum">
                    <li class="tab">
                        <a href="#all" class="active" name="all">Все</a>
                    </li>

                    <?php
                    $categories = get_terms( array(
	                    'taxonomy' => 'estate_cat',
                    ) );

                    if( $categories ){
	                    foreach( $categories as $cat ){
                            ?>
                            <li class="tab">
                                <a href="#<?php echo $cat->slug ?>"><?php echo $cat->name; ?></a>
                            </li>
                            <?php
	                    }
                    }
                    ?>
                </ul>
            </div>
            <!-- Содержимое каждой вкладки имеет id в соответствии с якорной ссылкой -->
            <div class="tabs-content">
                <div id="all">
                    <div class="container changing-padb">
                        <div class="row justify-content-between">


                                <?php
                                $args = array(
	                                'numberposts' => 6,
                                    'post_type' => 'estate'
                                );

                                $posts = get_posts( $args );

                                foreach($posts as $post){ setup_postdata($post);
	                                get_template_part('inc/postpreview2');
                                }

                                wp_reset_postdata();
                                ?>

                        </div>
                    </div>
                </div>
                            <?php
                            $categories = get_terms( array(
	                            'taxonomy' => 'estate_cat',
                            ) );

                            if( $categories ){

	                            foreach( $categories as $cat ){

                                    echo '<div id="' . esc_attr( $cat->slug ) . '">
                                            <div class="container changing-pad">
                                                <div class="row justify-content-start">';
                                                    $args = array(
                                                        'numberposts' => 9,
                                                        'post_type'   => 'estate',
                                                        'orderby'     => 'rand',
                                                        'tax_query' => array(
	                                                        array(
		                                                        'taxonomy' => 'estate_cat',
		                                                        'field'    => 'id',
		                                                        'terms'    => $cat->term_id
	                                                        )
                                                        )
                                                    );


                                                    $posts = get_posts( $args );

                                                    if($posts) {
	                                                    foreach($posts as $post){ setup_postdata($post);
		                                                    get_template_part('inc/postpreview2');
	                                                    }

	                                                    wp_reset_postdata();
                                                    }

                                            echo '</div>';
                                        echo '</div>';
                                    echo '</div>';

	                            }
                            };
                                ?>


            </div>
        </div>
</div>
    </div>
        <div class="flex-button">
            <a href="estate/" class="button aqua bt">Все предложения</a>
        </div>
    </div>
    </section>




        <!--menu-->
    <section class=" grayback">
        <div class="container">
    <div class="container c-m-b">
        <h2 class="zagolovok4">Коммерческая недвижимость</h2>
        <div class="menu-control">
            <ul class="button-group filters-button-group smallMenu smallMenu-w flex-colum">
                <li class="btn is-checked" data-filter="*">Все</li>
                <li class="btn" data-filter=".com_estate_cat-ready">Готовые обьекты</li>
                <li class="btn" data-filter=".com_estate_cat-building">Строящиеся объекты</li>
                <li class="btn" data-filter=".com_estate_cat-project">Проэкты</li>
            </ul>
        </div>
    </div>
        <div class="grid row">
	        <?php
	        $args = array(
		        'numberposts' => 6,
		        'post_type' => 'com_real_estate'
	        );

	        $posts = get_posts( $args );

	        foreach($posts as $post){ setup_postdata($post);
		        get_template_part('inc/postpreview3');
	        }

	        wp_reset_postdata();
	        ?>
        </div>
    <a href="/com_real_estate/" class="button aqua bt rise more">Все предложения</a>
        </div>
        </section>





    <section class="white">
        <div class="container margin-bottom-22">
            <div class="container change mrg-top3">
                <h2 class="zagolovok mrg-btm2">Свежие статьи</h2>
            </div>
            <div class="container changing-pad for-marg">
                <div class="row justify-content-around">

                    <?php if(have_posts()):
	                    while(have_posts()): the_post();
		                    $args = array(
			                    'numberposts' => 4,
			                    'category'    => 24,
			                    'orderby'     => 'rand',
			                    'order'       => 'DESC',
			                    'include'     => array(),
			                    'exclude'     => array(),
			                    'meta_key'    => '',
			                    'meta_value'  =>'',
			                    'post_type'   => 'post',
			                    'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
		                    );

		                    $posts = get_posts( $args );

		                    foreach($posts as $post){ setup_postdata($post);
			                    get_template_part('inc/postpreview4');
		                    }

		                    wp_reset_postdata();


	                    endwhile; endif; ?>

                </div>
                <a href="/statii/" class="button aqua mrg-btm3 btn-pad">Все статьи</a>
            </div>
            <!-- changing-pad -->
        </div>
        <div class="container">
            <div class="container">
                <div class="row justify-content-center gray big_text">
	                <?php
	                $post_id = 212;
	                $queried_post = get_post($post_id);
	                ?>
                    <h2><?php echo $queried_post->post_title; ?></h2>
	                <?php echo $queried_post->post_content; ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>