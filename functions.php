<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.06.2018
 * Time: 11:34
 */
/**
 * Enqueue scripts and styles.
 */
function patriot_scripts_enqueue() {
	wp_enqueue_style('bootstrap-grid', get_template_directory_uri(). '/css/bootstrap-grid.css');
	wp_enqueue_style('bootstrap-reboot.min', get_template_directory_uri() . '/css/bootstrap-reboot.min.css' );
	wp_enqueue_style('flexnav', get_template_directory_uri() . '/css/flexnav.css');
	wp_enqueue_style('fontawesome', get_template_directory_uri() . '/css/fontawesome.css');
	wp_enqueue_style('fontawesome.min', get_template_directory_uri() . '/css/fontawesome.min.css');
	wp_enqueue_style('fontawesome-all', get_template_directory_uri() . '/css/fontawesome-all.css');
	wp_enqueue_style('fontawesome-all.min', get_template_directory_uri() . '/css/fontawesome-all.min.css');
	wp_enqueue_style('magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css');
	wp_enqueue_style('nice-select', get_template_directory_uri() . '/css/nice-select.css');
	wp_enqueue_style('slick', get_template_directory_uri() . '/css/slick.css');
	wp_enqueue_style('style1', get_stylesheet_directory_uri() . '/style.css');
	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css', array(), '1.0.0', 'all' );

	wp_enqueue_style('responsive', get_template_directory_uri() . '/css/responsive.css');
	wp_enqueue_script('jquery.nice-select.min', get_template_directory_uri() . '/js/jquery.nice-select.min.js',array('jquery'),'1.0.0', true);

	wp_enqueue_script('flexnav', get_template_directory_uri() . '/js/flexnav.js',array('jquery'),'1.0.0', true);
	wp_enqueue_script('imagesloaded',array('jquery'),true);
	wp_enqueue_script('isotope.pkgd.min', get_template_directory_uri() . '/js/isotope.pkgd.min.js',array('jquery'),'1.0.0', true);
	wp_enqueue_script('jquery.magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.js',array('jquery'),'1.0.0', true);
	wp_enqueue_script('tab', get_template_directory_uri() . '/js/tab.js',array('jquery'),'1.0.0', true);
	wp_enqueue_script('slick.min', get_template_directory_uri() . '/js/slick.min.js',array('jquery'),'1.0.0', true);

	wp_enqueue_script('script', get_template_directory_uri() . '/js/script.js',array('jquery'),'1.0.0', true);
}
add_action('wp_enqueue_scripts', 'patriot_scripts_enqueue');

function patriot_theme_setup(){
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
}

add_action('after_setup_theme', 'patriot_theme_setup');

function register_my_menus(){
	register_nav_menus(array(
		'menu_header' => 'Menu in header',
		'menu_footer' => 'Menu in footer',
		'menu_social' => 'Social links'
	));
};

add_action('after_setup_theme', 'register_my_menus');

/**
 * Enable ACF 5 early access
 * Requires at least version ACF 4.4.12 to work
 */
define('ACF_EARLY_ACCESS', '5');

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

}

function patriot_highbar_setup(){/*Сайдбар у самому верху - не схована лише кнопка*/
	$args = array(
		'name'=>'Highbar',
		'id'=>'highbar',
		'description'=>'contact form in top'
	);
	register_sidebar($args);
}

add_action('widgets_init','patriot_highbar_setup');

function patriot_sidebar_setup(){
	$args = array(
		'name' => 'Sidebar',
		'id' => 'sidebar-1',
		'description' => __( 'Widget of website.'));
	register_sidebar($args);
}
add_action('widgets_init', 'patriot_sidebar_setup');

function patriot_footbar_setup(){/*Сайдбар у самому низу - з колонки у футері*/
	$args2 = array(
		'name'          => __('Footbar %d'),
		'id'            => 'footbar',
		'description'   => 'several footbars for footer'
	);
	register_sidebars(3, $args2);
}
add_action('widgets_init','patriot_footbar_setup');

function newsletter_sidebar(){
	$args3 = array(
		'name' => 'Newsletter',
		'id'=> 'newslet',
		'description' => 'get email for newsletter',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => "</div>\n",
	);
	register_sidebar($args3);
}

add_action('widgets_init','newsletter_sidebar');

add_filter( 'get_the_archive_title', function ($title) {
	if ( is_category() || is_tag() || is_tax() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_author() ) {
		$title = '<span class="vcard">' . get_the_author() . '</span>' ;
	} elseif ( is_post_type_archive() ) {
		$title = post_type_archive_title( '', false );
	}

	return $title;
});

