<footer>
	<div class="footer-yellback">
		<div class="container margin-bottom-0">
			<div class="row">
				<div class="col-sm-12 col-md-5">
					<h3 class="zagolovok3">Подписаться на рассылку</h3>
					<p class="big_text big_text-white">Будьте вкурсе
						<span class="hidden"> самых свежих</span> новостей наших объектов</p>
				</div>
				<div class="col-sm-12 col-md-7">

                        <?php dynamic_sidebar('newslet'); ?>

				</div>
			</div>
		</div>
	</div>
	<div class="footer-blackback">
		<div class="container flex-container margin-bottom-0">
			<div class="column">
                <ul>
				<?php dynamic_sidebar('footbar-1'); ?>
                </ul>
			</div>
			<div class="column column-padding">
                <ul>
					<?php dynamic_sidebar('footbar-2'); ?>
                </ul>
			</div>
			<div class="column column-padding2">
				<ul>
					<?php dynamic_sidebar('footbar-3'); ?>
				</ul>
			</div>
			<div class="column4">
				<div class="number1 number1-footer">
					<a href="tel:8&nbsp;(8482)&nbsp;61-37-37">8&nbsp;(8482)&nbsp;61-37-37</a>
				</div>
				<div class="number2 number2-footer">
					<a href="">dompatriot@gmail.com</a>
				</div>
				<p class="text-footer">Россия, г.Тольятти,
					<br> Приморский бульвар, д.57, подъезд 1</p>
				<a href="#forma" class="button red popup button-footer">Обратный звонок</a>
			</div>
		</div>
		<div class="container container-footer-padding no-margin">
			<div class="row justify-content-between">
				<div class=" col-md-8 col-xl-5">
					<p class="text-footer text-footer-margin"> 2018 ©
						<a href=""> Строительная компания «Патриот».</a> Все права защищены.
						<br> Жилая и коммерческая недвижимость в Тольятти от застройщика</p>
				</div>
				<div class="col-md-4 col-lg-3 align-self-center">

					<div class="row  no-gutters justify-content-between">
						<div class=" col-6 col-sm-4 col-md-8">
							<a href="https://vk.com/">
								<i class="fab fa-vk"> </i>
							</a>
							<a href="https://www.facebook.com/">
								<i class="fab fa-facebook-f"> </i>
							</a>
							<a href="https://twitter.com/">
								<i class="fab fa-twitter"> </i>
							</a>
						</div>
						<div class=" col-3 col-sm-2 col-md-4">
							<a href="#top">
								<i class="fas fa-chevron-up"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>


<?php wp_footer(); ?>
</body>

</html>


