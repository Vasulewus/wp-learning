<?php get_header(); ?>
    <div class="grayback">
        <div class="container margin-bottom-0 padding-bottom-38">
            <div class="row">
                <main class="col-lg-9">
                    <div class="main-text">
                        <div class="strichka">
                            <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
		                        <?php if(function_exists('bcn_display'))
		                        {
			                        bcn_display();
		                        }?>
                            </div>
                        </div>

                            <?php if(have_posts()):
                            while(have_posts()): the_post();
                            ?>
                        <h1><?php the_title();?></h1>
                        <div class="main-text-body">
                        <?php the_content();?>
                        </div>

                        <?php endwhile; endif; ?>
                        <h2 class="h1">Похожие записи</h2>
                        <div class="row">
                            <?php if(have_posts()):
                            while(have_posts()): the_post();
	                            $args = array(
		                            'numberposts' => 3,
		                            'category'    => 0,
		                            'orderby'     => 'rand',
		                            'order'       => 'DESC',
		                            'include'     => array(),
		                            'exclude'     => array(),
		                            'meta_key'    => '',
		                            'meta_value'  =>'',
		                            'post_type'   => 'post',
		                            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
	                            );

	                            $posts = get_posts( $args );

	                            foreach($posts as $post){ setup_postdata($post);
		                            get_template_part('inc/postpreview');
	                            }

	                            wp_reset_postdata();


 endwhile; endif; ?>
                        </div>
                    </div>
                </main>
                <aside class="col-lg-3 flex-boxing">
            <?php get_sidebar();?>
                </aside>
            </div>
        </div>
    </div>
<?php get_footer(); ?>