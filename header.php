<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,900&amp;subset=cyrillic" rel="stylesheet">
    <?php wp_head(); ?>
</head>


<body <?php body_class( ); ?>>

<header>
	<div class="header-top" id="top">
		<!--header-top-->
		<div class="container change">
			<div class="pack">
				<div class="row justify-content-around">
					<div class="col-12 col-md-8 col-lg-5">
						<div class="row no-gutters">
							<div class="col-12 col-sm-5">
								<div class="logo">
									<a href="<?php echo home_url(); ?>">
										<img src="<?php echo get_template_directory_uri();?>/img/patriot.png" alt="лого 'Патриот'">
									</a>
								</div>
							</div>
							<div class="col-12 col-sm-7">
								<div class="card0">
									Жилая и коммерческая недвижимость в Тольятти от застройщика
								</div>
							</div>
						</div>

					</div>
					<div class="col-12 col-sm-5 col-md-4 col-lg-4">
						<div class="number1 pad-left">
							<a href="tel:8&nbsp;(8482)&nbsp;61-37-37">8 (8482) 61-37-37</a>
							<a href="tel:8&nbsp;(8482)&nbsp;62-48-48">8 (8482) 62-48-48</a>
						</div>
						<div class="number2">
							<a href="mailto:dompatriot@gmail.com">dompatriot@gmail.com</a>
						</div>
					</div>
					<div class="col-12 col-sm-7 col-md-4 col-lg-3">
						<a href="#forma" class="button red popup" data-effect="mfp-zoom-in">Обратный звонок</a>
					</div>
				</div>
			</div>
		</div>
		<!-- header -container  -->
	</div>
	<!--/header-top-->
	<div class="header-bottom grayback">
		<!--header-bottom-->
		<div class="container cont1">
			<div class="row no-gutters justify-content-between">
				<div class="button blue menu-button">
					<div>МЕНЮ</div>
				</div>
				<div class="col-12 col-sm-7 col-md-4 col-lg-3">
					<a href="#forma" class="button red show popup" data-effect="mfp-zoom-in">Обратный звонок</a>
				</div>
                <?php wp_nav_menu( array(
                    'container' => false,
                    'theme_location'  => 'menu_header',
                    'menu_class'      => 'flexnav col-12 lg-screen',
                    'items_wrap' =>'<ul data-breakpoint="991" id="%1$s" class="%2$s">%3$s</ul>'
                )); ?>

			</div>
			<!--row-->
		</div>
		<!--container-->
	</div>
	<!--header-bottom-->
</header>
<!-- Popup itself -->
<div id="forma" class="white-popup mfp-with-anim mfp-hide">
	<div class="forma col-12">
     <ul>
			<?php dynamic_sidebar('highbar'); ?>
     </ul>
	</div>
</div>
